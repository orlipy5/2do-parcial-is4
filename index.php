<?php
define("TAMANO_MATRIZ", 4);

function generarMatriz() {
    $matriz = array();
    for ($i = 0; $i < TAMANO_MATRIZ; $i++) {
        for ($j = 0; $j < TAMANO_MATRIZ; $j++) {
            $matriz[$i][$j] = rand(0, 9);
        }
    }
    return $matriz;
}

function imprimirMatrizYCalcularDiagonal($matriz) {
    $sumaDiagonal = 0;
    echo "Matriz Generada:\n";
    for ($i = 0; $i < TAMANO_MATRIZ; $i++) {
        for ($j = 0; $j < TAMANO_MATRIZ; $j++) {
            echo $matriz[$i][$j] . "\t";
            if ($i == $j) {
                $sumaDiagonal += $matriz[$i][$j];
            }
        }
        echo "\n";
    }
    echo "Suma de la Diagonal Principal: $sumaDiagonal\n";
    return $sumaDiagonal;
}

do {
    $matrizGenerada = generarMatriz();
    $sumaDiagonal = imprimirMatrizYCalcularDiagonal($matrizGenerada);

    if ($sumaDiagonal >= 10 && $sumaDiagonal <= 15) {
        echo "La suma de la diagonal está entre 10 y 15. Fin del script.\n";
        break;
    } else {
        echo "La suma de la diagonal no está entre 10 y 15. Generando otra matriz...\n";
    }
} while (true);
?>
